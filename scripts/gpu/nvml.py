#!/usr/bin/python3
#SBATCH --gres=gpu:8 --nodelist=gpu-compute1 -p compsci-gpu
from py3nvml.py3nvml import *

nvmlInit()
deviceCount = nvmlDeviceGetCount()
for i in range(deviceCount):
    handle = nvmlDeviceGetHandleByIndex(i)
    procs = nvmlDeviceGetComputeRunningProcesses(handle)
    mem_used = round(float(nvmlDeviceGetMemoryInfo(handle).used/nvmlDeviceGetMemoryInfo(handle).total),2) * 100
    print(" % mem used:", mem_used, "GPU:", i)
    for p in procs:
        print("\tProcs:", p)
os.system('hostname')
