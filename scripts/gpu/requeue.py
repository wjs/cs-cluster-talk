#!/usr/bin/python3
#SBATCH --gres=gpu:2 -p compsci-gpu
import os
import py3nvml
import tensorflow as tf

job_id = os.getenv('SLURM_JOB_ID')
gpus = py3nvml.grab_gpus(num_gpus=2, gpu_select=[0,1,2,3], gpu_fraction=0.9)
if gpus == 2:
    # Creates a graph.
    config = tf.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 0.9
    session = tf.Session(config=config)
    # Creates a graph.
    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
    c = tf.matmul(a, b)
    ## Creates a session with log_device_placement set to True.
    sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    # Runs the op.
    print(sess.run(c))

else:
    os.system("scontrol requeue " + 'StartTime="now + 1 hours" ' + job_id)
    exit(0)

