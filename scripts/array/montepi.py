#!/usr/bin/python

import argparse
import glob
import math;
import os 
import random;

parser = argparse.ArgumentParser(description='MontePi')
parser.add_argument("-n", nargs='?', type=int, default=10000, help="number of digits?")
parser.add_argument('-c', '--calculate', action='store_true', help="calculate monte pi")
parser.add_argument('-s', '--summarize', action='store_true', help="print results")
args = parser.parse_args()

if args.calculate:
    task_id = os.getenv('SLURM_ARRAY_TASK_ID')
    count = circle = 0; 
    while count < int(args.n):
        x =  random.random();
        y =  random.random();
        if x*x + y*y <= 1:
            circle += 1
        count += 1
    rout = str(circle) + ":" + str(count)
    result = open('output/mp_' + task_id, 'w')
    result.write(rout)
    result.close()

if args.summarize:
    tcirc = tcount = 0
    circle = count = 0
    for i in sorted(glob.glob('output/mp_*')):
        circle, count = [int(i) for i in open(i).readlines()[0].split(':')]
        tcirc += circle
        tcount += count
    print("Estimated pi is: ", ((float(tcirc)/tcount) * 4), "Error is: ", round((((math.pi - ((float(tcirc)/tcount) * 4))/math.pi)*100), 5))
    print("Count: ", tcount, " Circle: ", tcirc)
