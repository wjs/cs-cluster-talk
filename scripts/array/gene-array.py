#!/usr/bin/python3
#SBATCH --array=1-4
import os
import re

SEQ = 'gattaca'
job_id = int(os.getenv('SLURM_ARRAY_TASK_ID'))
data = open('data_' + str(job_id) + '.txt').read()
if re.search(SEQ, data):
    print('Sequence found in file:', job_id)
