#!/usr/bin/python3
#SBATCH --array=1-4
import os

job_id = int(os.getenv('SLURM_ARRAY_TASK_ID')) - 1
a,b,c,d = [i.strip() for i in open('p.txt').readlines()[job_id].split(',')]
print(f'Running program with {a}, {b}, {c}, {d} for JOB_ID {job_id}')

