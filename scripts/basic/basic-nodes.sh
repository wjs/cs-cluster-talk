#!/bin/bash
# Limit to 5 minutes
#SBATCH -t 0:05:00  # time requested in hour:minute:second
# Output error and output to different location indexed by job id (%j)
#SBATCH -o /usr/xtmp/wjs/slurm-%j.out
#SBATCH -e /usr/xtmp/wjs/slurm-%j.err
# Request 100GB
#SBATCH --mem=100G
# Limit to these nodes
#SBATCH --nodelist=linux[41-44]
srun hostname
