#!/bin/bash
#SBATCH --job-name=wjs_test
# Limit running time to 5 minutes.
#SBATCH -t 0:05:00  # time requested in hour:minute:second
# Request 1GB or RAM
#SBATCH --mem=1G
hostname
sleep 300
