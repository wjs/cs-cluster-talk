#!/bin/bash
HOSTNAME=`hostname`
NPROC=`nproc`
srun echo "Running on $HOSTNAME with $NPROC processors"
